# _*_ coding: utf-8 _*_
#auth: haochenxiao
# Filename: mailer.py

from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText  
import smtplib  
import datetime,time
import os
import importlib,sys 
importlib.reload(sys)
from confs.Log import logger

class SendEmail:
	# 构造函数：初始化基本信息
	def __init__(self, host, user, passwd, port = 465):
		lInfo = user.split("@")
		self._user = user
		self._account = lInfo[0]
		self._me = self._account + "<" + self._user + ">" 
		
		#server = smtplib.SMTP()  
		server = smtplib.SMTP_SSL(host,port)
		server.connect(host)  
		#server.login(self._account, passwd)
		server.login(self._user, passwd)
		self._server = server	  
	
	# 发送文件或html邮件	
	def sendTxtMail(self, to_list, sub, content, subtype='html'):	
		# 如果发送的是文本邮件，则_subtype设置为plain
		# 如果发送的是html邮件，则_subtype设置为html
		msg = MIMEText(content, _subtype=subtype, _charset='utf-8')  
		msg['Subject'] = sub  
		msg['From'] = self._me  
		msg['To'] = ";".join(to_list)  
		try:
			self._server.sendmail(self._me, to_list, msg.as_string())   
			return True  
		except Exception as e:    
			print (e)
			return False
		
	# 发送带附件的文件或html邮件	   
	def sendAttachMail(self, to_list, sub, content, subtype='html'):
		# 创建一个带附件的实例
		msg = MIMEMultipart()  
		# 增加附件1
		att1 = MIMEText(open(r'D:\javawork\PyTest\src\main.py','rb').read(), 'base64', 'utf-8')
		att1["Content-Type"] = 'application/octet-stream'
		# 这里的filename可以任意写，写什么名字，邮件中显示什么名字
		att1["Content-Disposition"] = 'attachment; filename="main.py"'
		msg.attach(att1)
		
		# 增加附件2
		att2 = MIMEText(open(r'D:\javawork\PyTest\src\main.py','rb').read(), 'base64', 'utf-8')
		att2["Content-Type"] = 'application/octet-stream'
		att2["Content-Disposition"] = 'attachment; filename="main.txt"'
		msg.attach(att2)
		
		# 增加邮件内容
		msg.attach(MIMEText(content, _subtype=subtype, _charset='utf-8'))
		
		msg['Subject'] = sub  
		msg['From'] = self._me
		msg['To'] = ";".join(to_list)
		 
		try:
			self._server.sendmail(self._me, to_list, msg.as_string())   
			return True  
		except Exception as e:   
			return False
	 # 发送带附件的文件或html邮件	   
	def sendImageMail(self, to_list, sub, content, subtype='html'):
		# 创建一个带附件的实例
		msg = MIMEMultipart()
		
		# 增加邮件内容
		msg.attach(MIMEText(content, _subtype=subtype, _charset='utf-8'))
		
		# 增加图片附件
		image = MIMEImage(open(r'D:\javawork\PyTest\src\test.jpg','rb').read())
		#附件列表中显示的文件名
		image.add_header('Content-Disposition', 'attachment;filename=p.jpg')	 
		msg.attach(image)  
		
		msg['Subject'] = sub  
		msg['From'] = self._me
		msg['To'] = ";".join(to_list)
		
		try:
			self._server.sendmail(self._me, to_list, msg.as_string())   
			return True  
		except Exception as e:  
			return False
		
	# 析构函数：释放资源  
	def __del__(self):
		self._server.quit()
		self._server.close()


def SendMessages(mailto_list,ACCESS_LIST,ERROR_LIST,user,comments):
	#html 模板
	#######增加html模板
	h1='''
	<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title></title>
	<h1 align="center">运维管理系统</h1>
	<style type="text/css">
	body {margin:40px 30% 10px 15%;}
	</style>
	</head>
	</br></br></br></br>
	<p><font size="3" color="blue"><b>备注信息:</b></font></p>
	<body>
	'''

	h11 = '''
	<p><font size="3" color="blue"><b>%s</b></font></p>
		
	'''

	h2='''
	<p><font size="3" color="red">%s</font></p>
	'''
	h3='''
	<p><font size="3" color="green">%s</font></p>
	'''

	h4='''
	</br></br></br>
	<p><font size="3" color="green">发送时间: %s</font></p>
	<p><font size="3" color="green">执行人: %s</font></p>
	</body>
	</html>
	'''
	
	#业务层
	nowtime = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
	html = ''
	html = html + h1
	for j in comments.split('\n'):
		html += h11%(j)
	for m in ERROR_LIST:
		html += h2%(m)
	for n in ACCESS_LIST:
		html += h3%(n)
	html += h4%(nowtime,user)
	sub = '运维管理系统'
	mail = SendEmail(mail_host, mail_username, mail_password)
	if mail.sendTxtMail(mailto_list, sub, str(html)):  
		print("运维管理系统执行成功")
	else:  
		print("运维管理系统执行失败")


def SendtestMail(mail_host, mail_username, mail_password,mail_name,mail_port):

	sub = '运维管理系统'
	html = '<html>这是一封测试邮件,勿回! 发件人: %s</html>'%mail_name
	mailto_list = [mail_username]
	try:
		mail = SendEmail(mail_host, mail_username, mail_password,port=mail_port)
		mail.sendTxtMail(mailto_list, sub, str(html))
		logger.info("运维管理系统执行成功")
		return 0
	except Exception as e:
		logger.info(e)
		return 1

