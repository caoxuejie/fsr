# Generated by Django 2.0.6 on 2021-08-05 11:02

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('resources', '0006_serveruser_privatekey'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='disk',
            name='server',
        ),
        migrations.RemoveField(
            model_name='ip',
            name='server',
        ),
        migrations.RemoveField(
            model_name='server',
            name='idcs',
        ),
        migrations.RemoveField(
            model_name='server',
            name='product_host',
        ),
        migrations.RemoveField(
            model_name='server',
            name='product_one',
        ),
        migrations.RemoveField(
            model_name='server',
            name='product_two',
        ),
        migrations.RemoveField(
            model_name='server',
            name='server_auto',
        ),
        migrations.RemoveField(
            model_name='server',
            name='server_user',
        ),
        migrations.DeleteModel(
            name='Disk',
        ),
        migrations.DeleteModel(
            name='Idc',
        ),
        migrations.DeleteModel(
            name='Ip',
        ),
        migrations.DeleteModel(
            name='Server',
        ),
        migrations.DeleteModel(
            name='ServerAuto',
        ),
    ]
