# ~*~ coding: utf-8 ~*~
import sys
import json
import os
import importlib
import configparser
import requests
import socket
importlib.reload(sys)

#加载django
#import django
#CMDB_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
#sys.path.append(CMDB_PATH)
#os.environ.setdefault("DJANGO_SETTINGS_MODULE","syscmdb.settings")
#django.setup()


from paramiko_api import *
from mailers import SendMessages
from DingDing import SendMs
from configs import *
from mysqldb import DbSearch
from Log import logger,logger_task
import datetime
#from mysqldb import DbSearch

from celery import Celery,platforms
platforms.C_FORCE_ROOT = True
app = Celery('butterfly',backend=CELERY_RESULT_BACKEND,broker=BROKER_URL,include=['tasks'],)
app.conf.CELERY_IGNORE_RESULT = False
#json报错,注释了
#app.conf.CELERY_ACCEPT_CONTENT = [CELERY_ACCEPT_CONTENT]
app.conf.CELERY_TASK_SERIALIZER = CELERY_TASK_SERIALIZER
app.conf.CELERY_RESULT_SERIALIZER = CELERY_RESULT_SERIALIZER
app.conf.CELERY_TIMEZONE = CELERY_TIMEZONE
app.conf.CELERYD_MAX_TASKS_PER_CHILD = 1
app.conf.CELERYD_CONCURRENCY = 1
app.conf.CELERYD_PREFETCH_MULTIPLIER = 1
app.conf.CELERY_TASK_RESULT_EXPIRES = 1200
#CELERY异步任务执行超时时间为600秒
app.conf.CELERYD_TASK_TIME_LIMIT = 600
app.conf.CELERY_TIMEZONE = 'Asia/Shanghai'


#日志
@app.task
def RemoteDispatch(ip_list,item_list,MSG):

	alert_type = MSG['alert_type']
	alert_type_link = MSG['alert_type_link']
	user = MSG['user']
	alarm = MSG['alarm']
	db = DbSearch()

	ACCESS_LIST = []
	ERROR_LIST = []

	#自愈处于维护状态,为了防止第一次自愈没有启动完成,第二次自愈开始导致异常报错。
	db.DevStatus(alarm,0)

	logger_task.info('开始执行')

	ip_num = len(ip_list)
	for n in range(0,ip_num):

		asset = db.Asset(int(ip_list[n]))
		action = db.AutoAction(int(item_list[n]))

		logger.info(asset)
		logger.info(action)

		sm = SSHConnection(asset)
		result = sm.run_cmd(action)
		ds = 0

		logger_task.info(result)
		hostname = asset['hostname']
		if ds == 0:
			stdout = '主机%s,监控项%s,自愈动作%s,执行成功!执行日志:%s'%(hostname,alarm,action,str(result['stdout']))
			errout = ''
		else:
			stdout = ''
			errout = '主机%s,监控项%s,自愈动作%s,执行失败!执行日志:%s' % (hostname,alarm,action,str(result))

		ACCESS_LIST.append(stdout)
		ERROR_LIST.append(errout)

	try:
		if alert_type == 1:
			SendMessages(alert_type_link, ACCESS_LIST, ERROR_LIST, user)
		if alert_type == 2:
			mes = ','.join(ACCESS_LIST) + ','.join(ERROR_LIST)
			atl = alert_type_link.split('|')[0]
			keyword = '关键词:' + alert_type_link.split('|')[1] + ', '
			mes = keyword + mes
			SendMs(atl, mes)
	except Exception as e:
		logger_task.info('邮件配置信息有误,请检查邮件服务器/账号/密码、端口等信息')
		logger_task.info(e)
	# 自愈状态设置正常
	db.DevStatus(alarm, 1)

@app.task
def PrometheusDispatch(ip,action,tma):
	db = DbSearch()
	logger_task.info('prometheus异步执行开始...')
	# 自愈时效性
	valid = db.AlertSilence()[3]
	#判断时效性
	ntm = datetime.datetime.now()
	tm = datetime.datetime.strptime(tma, '%Y-%m-%d %H:%M:%S')
	if (ntm - tm).seconds > valid * 60:
		logger_task.info('%s 在Celery执行自愈中超过当前时效性,不进行自愈动作'%ip)
		return

	asset = db.AssetIp(ip)
	logger_task.info(asset)
	logger_task.info(action)

	sm = SSHConnection(asset)
	result = sm.run_cmd(action)
	logger_task.info(result)





